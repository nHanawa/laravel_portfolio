@extends('layouts.FFXdatabase')

@section('title', 'FFXdatabase')

@section('contents')
  <div>
    <div>
      <form action="" method="post">
        @csrf
        <div>
          <label for="form_id">ID</label>
          <input type="text" name="item_id" id="form_id" value="{{$item_id}}" placeholder="ID">
        </div>
        <div>
          <label for="form_name">name</label>
          <input type="text" name="item_name" id="form_name" value="{{$item_name}}" placeholder="name">
        </div>
        <div>
          <label for="form_buy">buy</label>
          <input type="number" name="item_buy_orhigher" id="item_buy_orhigher" value="{{$item_buy_orhigher}}" placeholder="minimum price">
          <span>～</span>
          <input type="number" name="item_buy_orlower" id="item_buy_orlower" value="{{$item_buy_orlower}}" placeholder="maximum price">
        </div>
        <div>
          <label for="form_buy">sell</label>
          <input type="number" name="item_sell_orhigher" id="item_sell_orhigher" value="{{$item_sell_orhigher}}" placeholder="minimum price">
          <span>～</span>
          <input type="number" name="item_sell_orlower" id="item_sell_orlower" value="{{$item_sell_orlower}}" placeholder="maximum price">
        </div>
        <div>
          <label for="form_enemy">drop_enemy</label>
          <input type="text" name="item_drop_enemy" id="item_drop_enemy" value="{{$item_drop_enemy}}" placeholder="drop enemy name">
        </div>
          <input type="submit" name="search" value="search">
          <input type="submit" name="clear" value="clear">
      </form>
    </div>
  </div>
  <table>
    <div>検索結果</div>
    <tr>
      <th>
        id
        <form action="" method="post">
          @csrf
          <button type="submit" name="sort[item_id]" value="1">▲</button>
          <button type="submit" name="sort[item_id]" value="2">▼</button>
        </form>
      </th>
      <th>
        item_name
        <form action="" method="post">
          @csrf
          <button type="submit" name="sort[item_name]" value="1">▲</button>
          <button type="submit" name="sort[item_name]" value="2">▼</button>
        </form>
      </th>
      <th>
        buy
        <form action="" method="post">
          @csrf
          <button type="submit" name="sort[buy]" value="1">▲</button>
          <button type="submit" name="sort[buy]" value="2">▼</button>
        </form>
      </th>
      <th>
        sell
        <form action="" method="post">
          @csrf
          <button type="submit" name="sort[sell]" value="1">▲</button>
          <button type="submit" name="sort[sell]" value="2">▼</button>
        </form>
      </th>
      <th>drop_enemy_name</th>
    </tr>
    @foreach ($items as $item)
    <tr>
      <td>{{$item->item_id}}</td>
      <td>{{$item->item_name}}</td>
      <td>{{$item->buy}}</td>
      <td>{{$item->sell}}</td>
      <td>
        @if ($item->enemies != null)
          @foreach($item->enemies as $val)
            {{$val->enemy_name}}
          @endforeach
        @endif
      </td>
    </tr>
    @endforeach
  </table>
@endsection

