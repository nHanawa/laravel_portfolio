<?php

require_once './DbManager.php';
require_once './scraping_item.php';

$ary_column = array_keys($item_list[1]);
//print_r($aryColumn);

//valueの中身以外を作成
$sql = "
  INSERT INTO
    items
    (item_id," . implode(',', $ary_column) . ")
  VALUES
";

$add_sql1 = [];

foreach($item_list as $key1 => $val1){
  $add_sql2 = [];
  $add_sql2[] = ":item_id" . $key1; 
  foreach($val1 as $key2 => $val2){
    $add_sql2[] = ":" . $key2 . $key1;
  }
  $add_sql1 []= "(" . implode(',', $add_sql2) . ")";
}

//sql文のvalue中身を追加
$sql .= implode(",", $add_sql1);

//sql文にON DUPLICATE KEY UPDATEを追加
$sql .= "
        ON DUPLICATE KEY UPDATE
        sell = VALUES(sell)
        , buy = VALUES(buy)
        ";

try {
  $db = getDB();
  $stt = $db->prepare($sql);
  //$stt->bindValue(":item_id1", "1");みたいな形を作っていく
  foreach($item_list as $key1 => $val1){
    $stt->bindValue(":item_id" . $key1, $key1);
    foreach($val1 as $key2 => $val2){
      $stt->bindValue(":" . $key2 . $key1, $val2);
    }
  }
  $stt->execute();
  print 'Data input is complete!';
} catch (PDOException $e) {
  print "Error Code: {$e->getCode()}"."\n";
  print "Error Message: {$e->getMessage()}";
} finally {
  $db =null;
}

/*
try {
  $db = getDb();

  $stt = $db->prepare($sql)
  $stt->bindvalue();
  $stt->ececute;
  print 'Data input is complete!';
} catch (PDOException $e) {
  print "Error Code: {$e->getCode()}"."\n";
  print "Error Message: {$e->getMessage()}";
} finally {
  $db = null;
}*/

/*
foreachを使わなかった場合
$stt = $db->prepare(
  'INSERT 
   INTO ffx_database(
     item_id,item_name
   )
   VALUES
   (:item_id1, :item_name1),
   (:item_id2, :item_name2),
   ...繰り返し
   );
$stt->bindValue(':item_id1', '1');
$stt->bindValue(':item_name1', 'ポーション');
$stt->bindValue...繰り返し
$stt->execute();
*/
?>