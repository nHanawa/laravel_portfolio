<?php
require_once('./phpQuery-onefile.php');

$html = file_get_contents('https://kyokugen.info/ff10/ff10_item.html');

$doc = phpQuery::newDocument($html);

sleep(3);

//分類を配列化
$category_doc = $doc["tr"]->find(".e7")->text();
$category = explode("\n", $category_doc);

//テキストファイルに出力(必要なくなったのでコメントアウト)
/*$file = fopen("category.txt", "w");
foreach($category as $key1 => $val1){    
    $write_in = $val1;
    fwrite($file, $write_in."\n");
}*/

//キー[textContent]のみ抽出し敵リストを配列で作成
$enemy = []; 
foreach($doc["tr"]->find(".e8") as $node){
  $enemy[] = $node->textContent;
}

$enemy_list = [];
//$categoryには最後空のデータが入ってるので、$enemyをcountする
for($i=0; $i<count($enemy); $i++){
  $enemy_list[] = ["$category[$i]" => "$enemy[$i]"];
}

//敵リストをtxtで作成
$file = fopen("enemy.txt", "w");
foreach($enemy_list as $key1 => $val1){
  foreach($val1 as $key2 => $val2){
    fwrite($file, $key2.":".$val2."\n");
  }
}
fclose($file);