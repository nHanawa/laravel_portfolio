<?php
//enemy.txtを再多次元配列化
$data = file("enemy.txt");
$enemy_list = [];
foreach($data as $val){
  $params = explode(":", $val);
  $enemy_list[] = [$params[0] => $params[1]];
}

//正規表現を使って、（）を中身含めて消す
//.*だけだと最長一致になるので、.*?で最短一致にする
foreach($enemy_list as $key1 => $val1){
  foreach ($val1 as $key2 => $val2) {
    $enemy_list[$key1][$key2] = preg_replace("/（.*?）/", "", $val2);
  }
}

//ドロップリスト作成
$drop_list = [];
foreach($enemy_list as $key1 => $val1){
  foreach($val1 as $key2 => $val2){
    if($key2 === "落とす"){
      if(preg_match("/ベヒーモスシン/", $val2)){
        $enemy_list[$key1][$key2] = preg_replace("/ベヒーモスシン/", "ベヒーモス、シン", $val2);
      }
      if(preg_match("/ or /", $val2)){
        $enemy_list[$key1][$key2] = preg_replace("/ or /", "、", $val2);
      }
      $param = explode("、", $enemy_list[$key1][$key2]);    
      $drop_list []= $param;
    }

  }  
}

//ドロップをtxtで作成（確認用
$file = fopen("drop.txt", "w");
foreach($drop_list as $key1 => $val1){
    fwrite($file, $key1."=>"."\n");
  foreach($val1 as $key2 => $val2){
    fwrite($file, $key2.":".$val2."\n");
  }
}
fclose($file);
