<?php

require_once './DbManager.php';
require_once './drop.php';
require_once './scraping_item.php';

$drop_item_name = [
  $item[1],
  $item[2],
  $item[3],
  $item[8],
  $item[9],
  $item[20],
  $item[43],
  $item[44],
  $item[51],
  $item[52],
  $item[56],
  $item[58],
  $item[64],
  $item[65],
  $item[67],
  $item[68],
  $item[69],
  $item[70],
  $item[71],
  $item[72],
  $item[73],
  $item[75],
  $item[78],
  $item[80],
  $item[81],
  $item[82],
  $item[83],
  $item[84],
  $item[85],
  $item[86],
  $item[87],
  $item[88],
  $item[89],
  $item[90],
  $item[91],
  $item[92],
  $item[93],
  $item[95],
  $item[96],
  $item[97],
  $item[98],
  $item[100],
  $item[104],
  $item[105],
  $item[106],
  $item[107],
  $item[108],
  $item[109],
  $item[110],
  $item[111],
];

$item_drop_list = array_combine($drop_item_name, $drop_list);

//アイテムー敵の名前対応表を0 => [アイテム => 敵]の形で作成
$item_enemy_list = [];
foreach($item_drop_list as $key1 => $val1){
  foreach($val1 as $key2 => $val2){
    $item_enemy_list[] = [$key1 => $val2];
  }
}

//INSERTとvalueの中身以外を作成
//item_name, enemy_nameがユニークなので、重複時無視するようにIGNORE
$sql = "
  INSERT IGNORE INTO
    name_drops
    (item_name, enemy_name)
  VALUES
";

//sql文のvalue中身を追加
/*
形は
VALUES
(:item_name1, :enemy_name1),
*/
$add_sql1 = [];

foreach($item_enemy_list as $key1 => $val1){
  $add_sql1[] = ":item_name".$key1.", :enemy_name".$key1;
}

$sql .= "(".implode("),(", $add_sql1).")";

try {
  $db = getDB();
  $stt = $db->prepare($sql);
  //$stt->bindValue(":item_name0", "チョコボ")の形で作る
  foreach($item_enemy_list as $key1 => $val1){
    foreach($val1 as $key2 => $val2){
      $stt->bindValue(":item_name".$key1, "$key2");
      $stt->bindValue(":enemy_name".$key1, "$val2");
    }
  }
  $stt->execute();
  print 'Data input is complete!';
} catch (PDOException $e) {
  print "Error Code: {$e->getCode()}"."\n";
  print "Error Message: {$e->getMessage()}";
} finally {
  $db =null;
}