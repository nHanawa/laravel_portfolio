<?php
function getDb() {
  $dsn = 'mysql:dbname=ffx_database; host=localhost; charset=utf8';
  $usr = 'root';
  $passwd = '';

  $db = new PDO($dsn, $usr, $passwd, [PDO::ATTR_PERSISTENT => true]);
  $db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
  return $db;
}