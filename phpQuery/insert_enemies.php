<?php

require_once './DbManager.php';
require_once './drop.php';

//valueの中身以外を作成
//enemy_nameがユニークなので、重複時無視するようにIGNORE
$sql = "
  INSERT IGNORE INTO
    enemies
    (enemy_name)
  VALUES
";

//sql文のvalue中身を追加しながら、敵一覧を$drop_enemy_listに１次元配列で纏め直す
$add_sql1 = [];
$drop_enemy_list = [];
foreach($drop_list as $key1 => $val1){
  foreach($val1 as $key2 => $val2){
    $add_sql1[] = ":enemy_name";
    $drop_enemy_list[] = $val2; 
  }
}
foreach($add_sql1 as $key1 => $val1){
  $add_sql2[] = $val1.$key1;
}
$sql .= "(".implode("),(", $add_sql2).")";


try {
  $db = getDB();
  $stt = $db->prepare($sql);
  //$stt->bindValue(":enemy_name0", "チョコボ")の形で作る
  foreach($drop_enemy_list as $key => $val){
    $stt->bindValue($add_sql2[$key], $val);
  }
  $stt->execute();
  print 'Data input is complete!';
} catch (PDOException $e) {
  print "Error Code: {$e->getCode()}"."\n";
  print "Error Message: {$e->getMessage()}";
} finally {
  $db =null;
}

