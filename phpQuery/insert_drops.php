<?php
require_once './DbManager.php';

$sql = "
  SELECT
    items.item_id
    ,enemies.enemy_id
  FROM
    name_drops
      JOIN
        items
      ON
        name_drops.item_name = items.item_name
      JOIN
        enemies
      ON
        name_drops.enemy_name = enemies.enemy_name
  ORDER BY
    name_drops.name_drop_id
";

try {
  $db =getDb();
  $stt = $db->prepare($sql);
  $stt->execute();
  //0 => ["item_id" => "1", "enemy_id" => "1"]の形で配列化
  $drops_list = $stt->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
  print "Error Code: {$e->getCode()}"."\n";
  print "Error Message: {$e->getMessage()}";
} finally {
  $db =null;
}

$sql = "
  INSERT IGNORE INTO
    drops
    (item_id, enemy_id)
  VALUES
";

//sql文のvalue中身を追加
/*
形は
VALUES
(:item_id1, :enemy_id1),
*/
$add_sql1 = [];
foreach($drops_list as $key1 => $val1){
    $add_sql1[] = ":item_id".$key1.", :enemy_id".$key1;
}

$sql .= "(".implode("),(", $add_sql1).")";

try {
  $db = getDb();
  $stt = $db->prepare($sql);
  //$stt->bindValue(":item_id0", "0")の形で作る
  foreach($drops_list as $key1 => $val1){
    foreach($val1 as $key2 => $val2){
      $stt->bindValue(":".$key2.$key1, "$val2");
    }
  }
  $stt->execute();
  print "Data input is Completed!";
} catch(PDOException $e) {
  print "Error Code: {$e->getCode()}"."\n";
  print "Error Message: {$e->getMessage()}";
} finally{
  $db = null;
}