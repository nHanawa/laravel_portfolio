<?php

namespace App\Http\Actions;

use App\Http\Controllers\Controller;
use App\Models\Item;

class Index extends Controller{
  public function __invoke(){
    $items = Item::all();
    $param = [
        'items' => $items, 
        'item_id' => '',
        'item_name' => '',
        'item_buy_orhigher' =>'',
        'item_buy_orlower' => '',
        'item_sell_orhigher' => '',
        'item_sell_orlower' => '',
        'item_drop_enemy' => '',
    ];
    return view('index.index', $param);
  }

}


