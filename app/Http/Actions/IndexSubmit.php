<?php

namespace App\Http\Actions;

use App\Http\Controllers\Controller;
use App\Http\Requests\IndexSubmitRequest;
use App\Models\Item;

class IndexSubmit extends Controller {
  public function __invoke(IndexSubmitRequest $request)
  {   
    if(!$request->isClear()){
        //searchボタンを押したときの処理（検索結果を表示する
        $item = new Item;
        $items = $item->search($request);
        $param = [ 
            'items' => $items, 
            'item_id' => $request->itemId(),
            'item_name' => $request->itemName(),
            'item_buy_orhigher' => $request->itemBuyOrHigher(),
            'item_buy_orlower' => $request->itemBuyOrLower(),
            'item_sell_orhigher' => $request->itemSellOrHigher(),
            'item_sell_orlower' => $request->itemSellOrLower(),
            'item_drop_enemy' => $request->itemDropEnemy(),
        ];        
    } else {
        //clearボタンを押したときの処理（検索内容を消去して全部表示する
        $items = Item::all();
        $param = [
            'items' => $items, 
            'item_id' => '',
            'item_name' => '',
            'item_buy_orhigher' =>'',
            'item_buy_orlower' => '',
            'item_sell_orhigher' => '',
            'item_sell_orlower' => '',
            'item_drop_enemy' => '',
        ];
    }
    return view('index.index', $param);
  }
}