<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Item;

class IndexController extends Controller
{
    public function submit(Request $request)
    {   
        if($request->has('search')){
            //searchボタンを押したときの処理（検索結果を表示する
            $item = new Item;

            $items = $item->search($request);

            $param = [ 
                'items' => $items, 
                'item_id' => $request->input('item_id'),
                'item_name' => $request->input('item_name'),
                'item_buy_orhigher' => $request->input('item_buy_orhigher'),
                'item_buy_orlower' => $request->input('item_buy_orlower'),
                'item_sell_orhigher' => $request->input('item_sell_orhigher'),
                'item_sell_orlower' => $request->input('item_sell_orlower'),
            ];

            return view('index.index', $param);
            
        } elseif ($request->has('clear')){
            //clearボタンを押したときの処理（検索内容を消去して全部表示する
            $items = Item::all();
            $param = [
                'items' => $items, 
                'item_id' => '',
                'item_name' => '',
                'item_buy_orhigher' =>'',
                'item_buy_orlower' => '',
                'item_sell_orhigher' => '',
                'item_sell_orlower' => $request->input('item_sell_orlower'),
            ];
            return view('index.index', $param);
        }
    }

}
