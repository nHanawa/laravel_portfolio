<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

class IndexSubmitRequest {
  
  //中の値を変更できないようにprivate
  private $itemId;
  private $itemName;
  private $itemBuyOrHigher;
  private $itemBuyOrLower;
  private $itemSellOrHigher;
  private $itemSellOrLower;
  private $itemDropEnemy;

  private $isClear;

  private $sort;

  //コンストラクタ―
  public function __construct()
  {
    //リクエストされてきた値を取る
    $request = Request::capture();

    $this->itemId = $request->input('item_id');
    $this->itemName = $request->input('item_name');
    $this->itemBuyOrHigher = $request->input('item_buy_orhigher');
    $this->itemBuyOrLower = $request->input('item_buy_orlower');
    $this->itemSellOrHigher = $request->input('item_sell_orhigher');
    $this->itemSellOrLower = $request->input('item_sell_orlower');
    $this->itemDropEnemy = $request->input('item_drop_enemy');

    if($request->has('search')){
      $this->isClear = false;
    } elseif ($request->has('clear')){
      $this->isClear = true;
    }

    $this->sort = $request->input('sort');
  }

  public function itemId(){
    return $this->itemId;
  }
  public function itemName(){
    return $this->itemName;
  }
  public function itemBuyOrHigher(){
    return $this->itemBuyOrHigher;
  }
  public function itemBuyOrLower(){
    return $this->itemBuyOrLower;
  }
  public function itemSellOrHigher(){
    return $this->itemSellOrHigher;
  }
  public function itemSellOrLower(){
    return $this->itemSellOrLower;
  }
  public function itemDropEnemy(){
    return $this->itemDropEnemy;
  }
  public function isClear(){
    return $this->isClear;
  }
  public function sort(){
    return $this->sort;
  }
}