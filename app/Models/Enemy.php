<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enemy extends Model 
{
  use HasFactory;

      //primaryKeyを明示的に指定
      protected $primaryKey = 'enemy_id';

      public function items(){
        return $this->BelongsToMany(Item::class, 'drops', 'enemy_id', 'item_id');
      }
}