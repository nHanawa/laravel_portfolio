<?php

namespace App\Models;

use App\Http\Requests\IndexSubmitRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    //primaryKeyを明示的に指定
    protected $primaryKey = 'item_id';

    public function enemies ()
    {
        //多対多リレーション: items-drops-enemies
        return $this->belongsToMany(Enemy::class, 'drops', 'item_id', 'enemy_id');
    }

    public function search (IndexSubmitRequest $request)
    {
        $query = Item::query();

        //検索
        if(!empty($request->itemId())){
            $query->where('item_id', $request->itemId());
        }
        if(!empty($request->itemName())){
            $query->where('item_name', 'like', '%'.$request->itemName().'%');
        }
        if(!empty($request->itemBuyOrHigher())){
            $query->where('buy', '>=', $request->itemBuyOrHigher());
        }
        if(!empty($request->itemBuyOrLower())){
            $query->where('buy', '<=', $request->itemBuyOrLower());
        }
        if(!empty($request->itemSellOrHigher())){
            $query->where('sell', '>=', $request->itemSellOrHigher());
        }
        if(!empty($request->itemSellOrLower())){
            $query->where('sell', '<=', $request->itemSellOrLower());
        }
        if(!empty($request->itemDropEnemy())){
            $query->wherehas('enemies', function($q) use($request){ 
                $q->where('enemy_name', 'like', '%'.$request->itemDropEnemy().'%');
            });
        }

        //ソート
        $sort = $request->sort();
        if(!empty($sort)){
            //id
            if(!empty($sort['item_id'])){
                if($sort['item_id'] === '1'){
                    $query->orderBy('item_id', 'asc');
                } elseif ($sort['item_id'] === '2'){
                    $query->orderBy('item_id', 'desc');
                }
            }
            //name
            if(!empty($sort['item_name'])){
                if($sort['item_name'] === '1'){
                    $query->orderBy('item_name', 'asc');
                } elseif ($sort['item_name'] === '2'){
                    $query->orderBy('item_name', 'desc');
                }
            }
            //buy
            if(!empty($sort['buy'])){
                if($sort['buy'] === '1'){
                    $query->orderBy('buy', 'asc');
                } elseif ($sort['buy'] === '2'){
                    $query->orderBy('buy', 'desc');
                }
            }
            //sell
            if(!empty($sort['sell'])){
                if($sort['sell'] === '1'){
                    $query->orderBy('sell', 'asc');
                } elseif ($sort['sell'] === '2'){
                    $query->orderBy('sell', 'desc');
                }
            }

        }

        return $query->get();
    }


}
